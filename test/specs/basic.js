const expect = require('chai').expect;
const Homepage = require('../../page/homepage.js');
const InitialAssessmentPage = require('../../page/initial_assessment_page.js');
const Questionare = require('../../page/questionare.js');
const PatientInfo = require('../../page/patient_info_page.js');
const Transition = require('../../page/transition_page.js');
const TherapistAssignment = require('../../page/therapist_assigment_page.js');
const CallSchedule = require('../../page/call_schedule_page.js');
const PersonalInfo = require('../../page/personal_info_page.js');
const Success = require('../../page/success_page.js');
const Page = require('../../page/page.js');

describe('Navigate the onboarding site and perform visual regression tests at screen level', () => {
    const homepage = new Homepage();
    const initialAssessmentPage = new InitialAssessmentPage();
    const page = new Page();
    const questionare = new Questionare();
    const patientInfo = new PatientInfo();
    const transition = new Transition();
    const therapistAssignment = new TherapistAssignment();
    const callSchedule = new CallSchedule();
    const personalInfo = new PersonalInfo();
    const success = new Success();

    it('opens the onboarding homepage', () => {
        page.open('c/sworddemo');
        homepage.waitUntilHomepageOpens();
    })
    it ('compares the homepage screen with the reference', () => {
        expect(browser.checkScreen('homepageExample')).equals(0);
    })
    it ('opens the first onboarding window', () => {
        homepage.clickGetStartedButton();
        initialAssessmentPage.waitUntilInitialAssessmentPageOpens();
        browser.pause(3000);
    });
    it('checks the initial assessment page status - initial', () => {
        expect(browser.checkScreen('initialAssessmentExample')).equals(0);
        expect(browser.checkElement($('.container-stepper'), 'screeningStepOneProgress')).equals(0);
    })
    it('clicks the Low Back Pain Therapy and visually re-checks page', () => {
        initialAssessmentPage.selectLowBackPainTherapy();
        initialAssessmentPage.waitUntilLowBackTherapyIsSelected();
        browser.pause(3000);
        expect(browser.checkElement($$('.condition-selector-container .condition-opt')[0], 'lowBackPainIconSelected')).equals(0);
        initialAssessmentPage.confirmTC();
        initialAssessmentPage.waitUntilContinueIsEnabled();
        initialAssessmentPage.clickContinueButton();
    })
    it('fills the therapy questions and checks the page visually', () => {
        questionare.waitUntilQuestionarePageOpens();
        questionare.selectPainDuration();
        questionare.selectNumbness();
        questionare.selectLackStrength();
        questionare.selectMedicationDuration();
        questionare.selectSurgery();
        questionare.selectNPS();
        initialAssessmentPage.waitUntilContinueIsEnabled();
        expect(browser.checkFullPageScreen('questionareExample')).equals(0);
        initialAssessmentPage.clickContinueButton();
    })
    it('fills the patient information checks the page visually', () => {
        patientInfo.waitUntilPatientInfoPageOpens();
        patientInfo.selectGender();
        patientInfo.birthDateMonth = 8;
        patientInfo.birthDateDay = 23;
        patientInfo.birthDateYear = 1991;
        patientInfo.heightFeet = 5;
        patientInfo.heightInch = 11;
        patientInfo.weight = 220;
        patientInfo.selectExerciseFrequency();
        initialAssessmentPage.waitUntilContinueIsEnabled();
        expect(browser.checkScreen('patientInfoExample')).equals(0);
        expect(browser.checkElement($('.container-stepper'), 'screeningStepOneProgress')).equals(0);
        initialAssessmentPage.clickContinueButton();
    })
    it('opens the transition page and checks it visually', () => {
        transition.waitUntilTransitionPageOpens();
        expect(browser.checkScreen('transitionExampleOne')).equals(0);
    })
    it('opens the therapist assignment checks the page and some key elements visually', () => {
        therapistAssignment.waitUntilTherapistAssignmentPageOpens();
        initialAssessmentPage.waitUntilContinueIsEnabled();
        expect(browser.checkScreen('therapistAssignmentExample')).equals(0);
        expect(browser.checkElement($('.assigned-pt img'), 'therapistPicture')).equals(0);
        expect(browser.checkElement($('.container-stepper'), 'screeningStepTwoProgress')).equals(0);
        initialAssessmentPage.clickContinueButton();
    })
    it('opens the video call schedule page and checks it visually', () => {
        callSchedule.waitUntilCallSchedulePageOpens();
        expect(browser.checkElement($('.container-stepper'), 'screeningStepThreeProgress')).equals(0);
        callSchedule.selectDayOfCall();
        callSchedule.selectTimeOfCall();
        initialAssessmentPage.waitUntilContinueIsEnabled();
        expect(browser.checkElement($('.video-call-time-selection'), 'callScheduleTimeSelected')).equals(0);
        initialAssessmentPage.clickContinueButton();
    })
    it('opens the personal info page, fill the form and checks it visually', () => {
        personalInfo.waitUntilPersonalInfoPageOpens();
        expect(browser.checkElement($('.container-stepper'), 'screeningStepFourProgress')).equals(0);
        personalInfo.name = "Onboarding Test";
        personalInfo.phone = 1234567890;
        personalInfo.email = "test@mail.com"
        personalInfo.address = "Rua das Flores, 35";
        personalInfo.city = "Porto";
        personalInfo.zipCode = 4000
        personalInfo.confirmTOS();
        initialAssessmentPage.waitUntilContinueIsEnabled();
        expect(browser.checkFullPageScreen('personalInfoExample')).equals(0);
        initialAssessmentPage.clickContinueButton();
    })
    it('opens the second transition page and checks it visually', () => {
        transition.waitUntilTransitionPageOpens();
        expect(browser.checkScreen('transitionExampleTwo')).equals(0);
    })
    it('opens the success page and checks it visually', () => {
        success.waitUntilSuccessPageOpens();
        expect(browser.checkScreen('successExample')).equals(0);
    })
})