const expect = require ('chai').expect;
const Page = require('../page/page');

class PersonalInfo extends Page {

    waitUntilPersonalInfoPageOpens(){
        super.waitForElement($('.personal-info'));
    }

    set name(value) {
        $('[name="name_field"]').setValue(value);
    }

    set phone(value) {
        $('.phone-number').setValue(value);
    }

    set email(value) {
        $('[name="sup_mail_name"]').setValue(value);
    }

    set address(value){
        $$('[placeholder="Enter your address"]')[0].setValue(value);
    }

    set city(value){
        $$('[placeholder="Enter your address"]')[1].setValue(value);        
    }

    set zipCode(value){
        $('[placeholder="Zip Code"]').setValue(value);        
    }

    confirmTOS(){
        super.actionClick($('[for="checkbox_terms_of_service"]'))
    }

}

module.exports = PersonalInfo;