const Page = require('../page/page');

class Success extends Page {

    waitUntilSuccessPageOpens(){
        super.waitForElement($('[alt="success"]'));
    }

    }

module.exports = Success;