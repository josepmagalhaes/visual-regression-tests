const expect = require ('chai').expect;
const Page = require('../page/page.js');

class CallSchedule extends Page {

    waitUntilCallSchedulePageOpens(){
        super.waitForElement($('.schedule-contact'));
    }

    selectDayOfCall() {
        super.actionClick($$('.question-choices')[0].$$('input')[0]);
    }

    selectTimeOfCall() {
        super.actionClick($$('.question-choices')[1].$$('input')[0]);
    }

}

module.exports = CallSchedule;