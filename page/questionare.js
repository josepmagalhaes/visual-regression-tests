const expect = require ('chai').expect;
const Page = require('../page/page');

class Questionare extends Page {

    waitUntilQuestionarePageOpens(){
        super.waitForElement($('.questions'));
    }

    selectPainDuration() {
        super.actionClick($$('[name="experience_pain"]')[0]);
    }

    selectNumbness() {
        super.actionClick($$('[name="legs_numbness"]')[0]);
    }

    selectLackStrength() {
        super.actionClick($$('[name="lack_strength_legs"]')[0]);
    }

    selectMedicationDuration() {
        super.actionClick($$('[name="lowback_medication"]')[0]);
    }

    selectSurgery() {
        super.actionClick($$('[name="had_hip_surgery"]')[0]);
    }

    selectNPS() {
        super.actionClick($$('.ruller .radio-button > .checker')[9]);
    }

}

module.exports = Questionare;