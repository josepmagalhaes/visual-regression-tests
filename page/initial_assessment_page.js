const expect = require ('chai').expect;
const Page = require('../page/page');

class InitialAssessmentPage extends Page {

    waitUntilInitialAssessmentPageOpens(){
        super.waitForElement($$('.container-stepper .step')[0]);
    }

    selectLowBackPainTherapy(){
        super.actionClick($$('.condition-selector-container .condition-opt')[0]);
    }

    confirmTC(){
        super.actionClick($('[for=checkbox_terms_conditions]'));
    }

    waitUntilLowBackTherapyIsSelected(){
        browser.waitUntil(() => {
            return $$('.condition-selector-container .condition-opt')[0].getAttribute('class').includes('active');
        })
    }

    waitUntilContinueIsEnabled() {
        browser.waitUntil(() => {
            return !$('.next-step').getAttribute('disabled');
        })
    }

    clickContinueButton() {
        super.actionClick($('.next-step'));
    }

}

module.exports = InitialAssessmentPage;