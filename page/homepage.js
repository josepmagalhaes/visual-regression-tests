const expect = require ('chai').expect;
const Page = require('../page/page');

class Homepage extends Page {

    waitUntilHomepageOpens(){
        super.waitForElement($('.company-logo'));
    }

    clickGetStartedButton() {
        super.actionClick($('.mt-5.mb-5.mb-md-0'));
    }

}

module.exports = Homepage;