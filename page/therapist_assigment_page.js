const expect = require ('chai').expect;
const Page = require('../page/page');

class TherapistAssignment extends Page {

    waitUntilTherapistAssignmentPageOpens(){
        super.waitForElement($('.assigned-pt'));
    }

}

module.exports = TherapistAssignment;