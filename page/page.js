class Page {

    constructor() {
        browser.fullscreenWindow();
    }

    open(path){
        browser.url('/' + path);
    }

    waitForElement(selector){
        browser.waitUntil(() => {
            return selector.isDisplayed();
        }, 10000);
    }

    actionClick(selector){
        return selector.click();
    }
}

module.exports = Page;