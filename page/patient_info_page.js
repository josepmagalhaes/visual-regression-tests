const expect = require ('chai').expect;
const Page = require('../page/page');

class PatientInfo extends Page {

    waitUntilPatientInfoPageOpens(){
        super.waitForElement($('.patient-characteristics-container'));
    }

    selectGender() {
        super.actionClick($$('[name="$v.gender.$model"]')[0]);
    }

    set birthDateMonth(value) {
        $$('.data-field > input')[0].setValue(value);
    }

    set birthDateDay(value) {
        $$('.data-field > input')[1].setValue(value);
    }

    set birthDateYear(value) {
        $$('.data-field > input')[2].setValue(value);
    }

    set heightFeet(value){
        $$('.patient-characteristics-container .field-container')[2].$$('input')[0].setValue(value);
    }

    set heightInch(value){
        $$('.patient-characteristics-container .field-container')[2].$$('input')[1].setValue(value);        
    }

    set weight(value){
        $$('.patient-characteristics-container .field-container')[3].$('input').setValue(value);        
    }

    selectExerciseFrequency(){
        super.actionClick($$('[name="exercise_days-per-week"]')[0])
    }

}

module.exports = PatientInfo;